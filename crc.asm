.286

code segment

program:
	mov ax, stack	; inicjowanie stosu
	mov ss, ax
	mov sp, offset czubek
	
	mov ax, data	; inicjowanie danych
	mov ds, ax
	
	call fWczytajParametry
	call fSprawdzParametry
	
	; sprawdzenie ilo�ci argument�w
	mov ax, word ptr ds:[zArgc]
	cmp ax, 2
		je programWyliczanie
	
	programWeryfikacja:
		; weryfikacja sum kontrolnych (opcja -v)
		call fWeryfikuj
		jmp ProgramEnds
	programWeryfikacjaEnd:
	
	programWyliczanie:
		; wyliczanie sum kontrolnych (bez opcji)
		call fWylicz
	programWyliczenieEnd:
	
	programEnds:
	
	call fZamknijPliki
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4C00h
	int 21h
programEnd:

fBialyZnak proc	; in: al - znak, cx - ilo�� znak�w do przeczytania
		; out: ah - 0 (bia�y znak), 1 (nie-bia�y znak)
	cmp cx, 0
		je fBialyZnakTak
	cmp al, 20h	; SP
		je fBialyZnakTak
	cmp al, 00h	; \0
		je fBialyZnakTak
	cmp al, 09h	; \t
		je fBialyZnakTak
	cmp al, 0Ah	; \n
		je fBialyZnakTak
	cmp al, 0Dh	; \r
		je fBialyZnakTak
	
	fBialyZnakNie:
		mov ah, 1
		ret
	
	fBialyZnakTak:
		mov ah, 0
		ret
fBialyZnak endp

fNieBialyZnak proc
		; in: es:ax - adres w pami�ci, cx - maksymalna ilo�� bajt�w do przeczytania
		; out: es:ax - adres w pami�ci, cx - ilo�� bajt�w pozosta�a do przeczytania
	push si
	
	mov si, ax
	
	fNieBialyZnakPetla:
		mov al, byte ptr es:[si]
		
		call fBialyZnak
		cmp ah, 0
			je fNieBialyZnakBialy
		jmp fNieBialyZnakPetlaEnd
		
		fNieBialyZnakBialy:
			inc si
			loop fNieBialyZnakPetla
	fNieBialyZnakPetlaEnd:
	
	mov ax, si
	
	pop si
	ret	
fNieBialyZnak endp

fWczytajParametry proc
	pusha
	
	; ilo�� znak�w w parametrach linii komend
	xor cx, cx
	mov cl, byte ptr es:[80h]
	
	; pocz�tek parametr�w linii komend (es:81h)
	mov si, 81h
	
	; miejsce do zapisu parametr�w
	mov di, offset zArgv
	; miejsce do zapisu d�ugo�ci parametr�w
	mov bx, offset zArgl
	
	; czy mog� istnie� jakiekolwiek argumenty?
	cmp cl, 0
		je fWczytajParametryPetlaEnd
	
	; iteruj po kolejnych znakach linii komend
	fWczytajParametryPetla:
		; przejd� do pierwszego nie-bia�ego znaku
		mov ax, si
		call fNieBialyZnak
		mov si, ax
		
		; lArgl = 0 - ilo�� znak�w w aktualnym parametrze
		xor dx, dx
		
		; iteruj po kolejnych znakach parametru linii komend
		fWczytajParametryParametr:
			; al = aktualny znak parametru
			mov al, byte ptr es:[si]
			inc si
			
			; if whitespace(al): break;
			call fBialyZnak
			cmp ah, 0
				je fWczytajParametryPatametrEnd
			
			; zArgv[zArgc][di - zArgv[zArgc]] = al
			mov byte ptr ds:[di], al
			; di++
			inc di
			; lArgl++
			inc dx
			
			loop fWczytajParametryParametr
		fWczytajParametryPatametrEnd:
		
		; ostatnio przetwarzany parametr by� pusty - koniec
		cmp dx, 0
			je fWczytajParametryPetlaEnd
		
		; zArgl[bx] = lArgl
		mov word ptr ds:[bx], dx
		add bx, 2	; bx++
		
		; zArgc++
		mov ax, word ptr ds:[zArgc]
		inc ax
		mov word ptr ds:[zArgc], ax
		
		; osi�gni�to maksymaln� ilo�� parametr�w - koniec
		cmp ax, 16
			je fWczytajParametryPetlaEnd
		
		; wyczerpano ilo�� znak�w do przeczytania - koniec
		cmp cx, 0
			je fWczytajParametryPetlaEnd
		
		loop fWczytajParametryPetla
	fWczytajParametryPetlaEnd:
	
	popa
	ret
fWczytajParametry endp

fSprawdzParametry proc
	push ax
	
	; dwa lub trzy parametry
	mov ax, word ptr ds:[zArgc]
	cmp ax, 2
		jl fSprawdzParametryBladIlosc
		je fSprawdzParametry3End
	cmp ax, 3
		jg fSprawdzParametryBladIlosc
	
	; je�li trzy, to pierwszym z nich ma by� -v
	fSprawdzParametry3:
		mov ax, word ptr ds:[zArgl]
		cmp ax, 2 	; "-v" - dwa znaki
			jne fSprawdzParametryBlad3ArgArg1
		mov ax, word ptr ds:[zArgv]
		cmp ax, 762Dh	; "v-" (little endian)
			jne fSprawdzParametryBlad3ArgArg1
	fSprawdzParametry3End:
	
	pop ax
	ret
	
	fSprawdzParametryBlad3ArgArg1:
		mov ax, offset zBlad3ArgArg1
		call fBlad
	
	fSprawdzParametryBladIlosc:
		mov ax, offset zBladIloscArgumentow
		call fBlad
fSprawdzParametry endp

fSprawdzHex proc	; in: al - bajt do sprawdzenia
					; out: ah - 0 (hex), 1 (nie hex)
	xor ah,ah
	
	; if al < '0': exception
	cmp al, '0'
		jl fSprawdzHexError
	; if al <= '9': return
	cmp al, '9'
		jle fSprawdzHexOK
	; if al < 'A': exception
	cmp al, 'A'
		jl fSprawdzHexError
	; if al <= 'F': return
	cmp al, 'F'
		jle fSprawdzHexOK
	; if al < 'a': exception
	cmp al, 'a'
		jl fSprawdzHexError
	; if al <= 'f': return
	cmp al, 'f'
		jle fSprawdzHexOK
	
	fSprawdzHexError:
		inc ah
	
	fSprawdzHexOK:
		ret	
fSprawdzHex endp

fBajtNaHex proc	; in; al - liczba
				; out: ah, al - znaki ASCII
	mov ah, al
	
	; bardziej znacz�cy p�bajt
	shr ah, 4
	add ah, '0'
	cmp ah, '9'
		jle fBajtNaHexH
	add ah, 7	; 'A' - '9' + 1
	
	fBajtNaHexH:
	
	; mniej znacz�cy p�bajt
	and al, 00001111b
	add al, '0'
	cmp al, '9'
		jle fBajtNaHexL
	add al, 7	; 'A' - '9' + 1
	
	fBajtNaHexL:
	
	ret
fBajtNaHex endp

fHexNaLiczbe proc	; in: bh, bl - znaki heksadecymalne
					; out: CF - b��d?, ax - wynik
	; wczytaj pierwszy p�bajt
	xor ax, ax
	mov al, bh
	
	push ax
	
	; sprawd� bardziej znacz�cy p�bajt
	call fSprawdzHex
	cmp ah, 0
		jne fHexNaLiczbeBlad
	
	pop ax
	
	; przelicz na liczb�
	sub al, '0'
	cmp al, 10
		jl fHexNaLiczbe1Ok
	sub al, 7	; 'A' - '9' + 1
	cmp al, 16
		jl fHexNaLiczbe1Ok
	sub al, 32	; 'a' - 'F' + 1

	fHexNaLiczbe1Ok:
	
	; przesu� wynik na cztery najbardziej znacz�ce bity ax
	ror ax, 4
	
	; wczytaj drugi p�bajt
	mov al, bl
	
	push ax
	
	; sprawd� mniej znacz�cy p�bajt
	call fSprawdzHex
	cmp ah, 0
		jne fHexNaLiczbeBlad
	
	pop ax
	
	; przelicz na liczb�
	sub al, '0'
	cmp al, 10
		jl fHexNaLiczbe2Ok
	sub al, 7	; 'A' - '9' + 1
	cmp al, 16
		jl fHexNaLiczbe2Ok
	sub al, 32	; 'a' - 'F' + 1
	
	fHexNaLiczbe2Ok:
	
	; "dodaj" wyniki
	or al, ah
	xor ah, ah
	
	clc
	ret
	
	fHexNaLiczbeBlad:
		pop ax
		stc
		ret
fHexNaLiczbe endp

fWypiszLiczbe proc	; in: ax - liczba
	push di
	push dx
	push bx
	push ax
	
	; dzielnik
	mov bx, 10
	
	; miejsce zapisu liczby
	mov di, offset zLiczbaWypisz
	add di, 5
	
	fWypiszLiczbePetla:
		; wyczy�� dx
		xor dx, dx
		; ax = dx:ax / bx (reszta z dzielenia w dx)
		div bx
		
		; dx += '0'
		add dx, '0'
		
		; zapisz cyfr� wyniku
		dec di
		mov byte ptr ds:[di], dl
		
		cmp ax, 0
			jne fWypiszLiczbePetla
	fWypiszLiczbePetlaEnd:
	
	; int 21h ah=09h - wypisywanie stringa$
	mov ax, 0900h
	mov dx, di
	int 21h
	
	pop ax
	pop bx
	pop dx
	pop di
	ret
fWypiszLiczbe endp

fOtworzPlik proc	; in: ax - typ otwarcia; cx - d�ugo�� nazwy pliku;
					;     dx - offset do nazwy pliku; bp - bufor
	pusha
	
	; dla obs�ugi b��d�w
	mov bx, cx
	
	; kopiuj cx bajt�w z dx do zOtworzPlikNazwa
	mov si, dx
	
	; es = ds (movsb: ds:si -> es:di)
	mov dx, ds
	mov es, dx
	
	; przekopuj nazw� pliku do zOtworzPlikNazwa
	mov di, offset zOtworzPlikNazwa
	rep movsb
	
	; dodaj NUL na koniec zOtworzPlikNazwa
	xor dx, dx
	mov byte ptr ds:[di], dl
	
	; int 21h ah=3C|5B/3Dh - otwieranie/tworzenie pliku
	; mov al, 	; opcje (do odczytu/do zapisu)
	xor cx, cx	; zerujemy opcje
	mov dx, offset zOtworzPlikNazwa
	int 21h
	jc fOtworzPlikBlad
	
	; bBuforUchwyt = uchwyt
	mov word ptr ds:[bp+2], ax
	
	; lista otwartych plik�w
	mov bx, word ptr ds:[zOtwartyPlik]
	mov word ptr ds:[bx+zOtwartePliki], ax
	add bx, 2
	mov word ptr ds:[zOtwartyPlik], bx
	
	popa
	ret
	
	fOtworzPlikBlad:	; in: ax - kod b��du,
						;     bx - d�ugo�� nazwy pliku, dx - offset do nazwy pliku
		; zamie� kod b��du na znaki ASCII (hex)
		call fBajtNaHex
		; zapami�tanie wyniku
		mov bp, ax
		
		push dx
		; int 21h ah=09h - wypisywanie stringa$
		mov ah, 09h
		mov dx, offset zBladOtworzPlik
		int 21h
		pop dx
		
		; wypisanie nazwy pliku
		; int 21h ah=40h - wypisywanie danych
		mov ah, 40h
		mov cx, bx		; liczba bajt�w do wypisania
		mov bx, 0		; uchwyt pliku	
		; mov dx, 		; dx - adres danych
		int 21h
		
		; �adne wypisywanie "plik: KOD"
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, ':'
		int 21h	
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, ' '
		int 21h	
		
		; wypisanie kodu b��du
		mov ax, bp
		; pierwszy znak
		xchg ah, al
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		; drugi znak
		mov ax, bp
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, 'h'
		int 21h	
		
		; przekazanie obs�ugi b��d�w wy�ej
		mov ax, offset zBladPlik
		call fBlad
	fOtworzPlikBladEnd:
fOtworzPlik endp

fOtworzPlikOdczyt proc	; in: cx - d�ugo�� nazwy pliku, dx - offset do nazwy pliku, bp - bufor
			; out: ax - file handle
	mov ax, 3D00h
	call fOtworzPlik
	ret
fOtworzPlikOdczyt endp

fOtworzPlikZapis proc	; in: cx - d�ugo�� nazwy pliku, dx - offset do nazwy pliku, bp - bufor
			; out: ax - file handle
	;mov ax, 5C00h
	mov ax, 3C00h
	call fOtworzPlik
	ret
fOtworzPlikZapis endp

fZamknijPliki proc
	push si
	push bx
	push ax
	
	; wska�nik do uchwyt�w plik�w
	mov si, word ptr ds:[zOtwartyPlik]
	cmp si, 0
		je fZamknijPlikiPetlaEnd
	
	; zamykaj pliki do ko�ca bufora
	fZamknijPlikiPetla:
		sub si, 2
		
		; int 21h, ah=3Eh - zamknij plik
		mov ax, 3E00h
		mov bx, word ptr ds:[si+zOtwartePliki]
		int 21h
		
		cmp si, 0
			jne fZamknijPlikiPetla
	fZamknijPlikiPetlaEnd:
	
	pop ax
	pop bx
	pop si
	ret
fZamknijPliki endp

fWczytajBufor proc	; in: bp - bufor
	pusha
	
	; bp - offset bBufor
	; si - bBuforCzytaj
	; di - bBuforZapisz
	
	mov si, word ptr ds:[bp+4]
	mov di, word ptr ds:[bp+6]
	
	; Oblicz ile bajt�w mo�na wczyta�
	cmp di, si
		jne fWczytajRet
	
	; int 21h, ah=3Fh - wczytaj z pliku
	mov ax, 3F00h
	mov bx, word ptr ds:[bp+2] 	; uchwyt pliku
	mov cx, word ptr ds:[bp]	; wielko�� bufroa
	mov dx, bp	; miejsce do zapisu
	add dx, 8
	int 21h
	jc fWczytajBuforBlad
	
	; Zapisz nowe warto�ci wska�nik�w
	mov word ptr ds:[bp+4], 0
	mov word ptr ds:[bp+6], ax
	
	fWczytajRet:
	popa
	ret
	
	fWczytajBuforBlad:	; in: ax - kod b��du, bx - uchwyt pliku
		call fBajtNaHex
		mov bp, ax
		
		; int 21h ah=09h - wypisywanie stringa$
		mov ah, 09h
		mov dx, offset zBladWczytajPlik
		int 21h
		
		; wypisz uchwyt pliku
		mov ax, bx
		shr ax, 8
		call fBajtNaHex
		mov si, ax
		
		; pierwszy hex z file handle
		mov ax, si
		shr ax, 8
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h	
		
		; drugi hex z file handle
		mov ax, si
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		mov ax, bx
		call fBajtNaHex
		mov si, ax
		
		; trzeci hex z file handle
		mov ax, si
		shr ax, 8
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h	
		
		; czwarty hex z file handle
		mov ax, si
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, ':'
		int 21h	
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, ' '
		int 21h	
			
		; pierwszy hex z kodu b��du
		mov ax, bp
		shr ax, 8
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h	
		
		; drugi hex z kodu b��du
		mov ax, bp
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, 'h'
		int 21h	
		
		mov ax, offset zBladPlik
		call fBlad
	fWczytajBuforBladEnd:
fWczytajBufor endp

fWczytajBuforZnak proc	; in: bp - bufor
						; out: ah - 0 (ok), 1 (koniec pliku); al - znak
	push si
	push di
	
	; bp - offset bBufor
	; si - bBuforCzytaj
	; di - bBuforZapisz
	mov si, word ptr ds:[bp+4]
	mov di, word ptr ds:[bp+6]
	
	; bufor pusty, spr�buj wype�ni� go danymi z pliku
	cmp si, di
		je fWczytajZnakDoczytaj
	
	; bufor zawiera znaki
	fWczytajZnakBuforGotowy:
	
	xor ah, ah
	
	; wczytaj znak
	mov al, byte ptr ds:[bp+8+si]
	inc si
	
	; zapisz now� pozycj� wska�nika odczytu
	mov word ptr ds:[bp+4], si
	
	fWczytajZnakRet:
	pop di
	pop si
	ret
	
	fWczytajZnakDoczytaj:
		; wype�niamy bufor
		call fWczytajBufor
		; od�wie�amy wska�niki zapisu/odczytu
		mov si, word ptr ds:[bp+4]
		mov di, word ptr ds:[bp+6]
		cmp si, di
			jne fWczytajZnakBuforGotowy
	
	fWczytajZnakKoniecPliku:
		; koniec pliku
		mov ax, 0100h
		jmp fWczytajZnakRet
fWczytajBuforZnak endp

fWczytajLinie proc	; in: bp - bufor
					; out: CF - b��d?, ax - kod b��du (CF=0: 0 - brak b��du, 1 - koniec pliku)
	push di
	
	mov di, offset bLinia
	
	fWczytajLiniePetla:
		call fWczytajBuforZnak
		cmp ah, 0	; mo�liwy koniec pliku
			jne fWczytajLiniePetlaEnd
		
		cmp al, 0Dh	; \r
			je fWczytajLiniePetlaCR
		
		; przekroczyliby�my rozmiar bufora?
		cmp di, offset bLinia + 16384
			je fWczytajLinieBladDlugosc
		
		; zapiszmy w pami�ci
		mov byte ptr ds:[di], al
		inc di
		
		jmp fWczytajLiniePetla
		
		fWczytajLiniePetlaCR: ; \r
			call fWczytajBuforZnak
			cmp ah, 0		; mo�liwy koniec pliku
				jne fWczytajLinieBladCR
			
			cmp al, 0Ah		; \n
				jne fWczytajLinieBladHex
	fWczytajLiniePetlaEnd:
	
	; zapisz ilo�� znak�w w linii
	sub di, offset bLinia
	mov word ptr ds:[bLiniaDlugosc], di
	
	; plik si� sko�czy�?
	cmp ah, 1
		je fWczytajLinieKoniecPliku
	
	xor ax, ax
	
	fWczytajLinieRet:
	pop di
	ret
	
	; Plik si� sko�czy�, ustaw flag�
	fWczytajLinieKoniecPliku:
		xor ax, ax
		inc ax
		jmp fWczytajLinieRet
	
	fWczytajLinieBladCR:
		mov ax, offset zBladZnakCR
		stc
		jmp fWczytajLinieRet
	
	; Znale�li�my znak nie-heksadecymalny
	fWczytajLinieBladHex:
		mov ax, offset zBladZnakHex
		stc
		jmp fWczytajLinieRet
	
	; Linia d�u�sza ni� bufor
	fWczytajLinieBladDlugosc:
		mov ax, offset zBladDlugoscLinii
		stc
		jmp fWczytajLinieRet
fWczytajLinie endp

fWyliczCRCLinia proc	; out: ax - CRC16
	push si
	push dx
	push cx
	
	; Ilo�� znak�w w linii
	mov cx, word ptr ds:[bLiniaDlugosc]
	
	; offset linii
	mov si, offset bLinia
	
	xor ax, ax
	
	cmp cx, 0
		je fWyliczCRCLiniaPetlaEnd
	
	fWyliczCRCLiniaPetla:
		push cx
		
		; Wczytaj znak z linii
		mov dl, byte ptr ds:[si]
		inc si
		
		; "Dodaj" go do wyniku
		xor al, dl
		
		; Ilo�� bit�w w znaku
		mov cx, 8
		
		; Sprawdzaj najbardziej znacz�ce bity
		fWyliczfWyliczCRCLiniaPetlaXOR:
			shr ax, 1
			jnc fWyliczfWyliczCRCLiniaPetlaXORDalej
			
			xor ax, 1010000000000001b ; (x^16 +) x^15 + x^2 + 1
			
			fWyliczfWyliczCRCLiniaPetlaXORDalej:
			loop fWyliczfWyliczCRCLiniaPetlaXOR
		fWyliczfWyliczCRCLiniaPetlaXOREnd:
		
		pop cx
		loop fWyliczCRCLiniaPetla
	fWyliczCRCLiniaPetlaEnd:
	
	pop cx
	pop dx
	pop si
	ret
fWyliczCRCLinia endp

fZapiszCRC proc		; ax - CRC, bp - bufor
	push dx
	push cx
	push bx
	push ax
	
	; miejsce do zapisu danych (bufor)
	mov bx, bp
	add bx, 8
	
	; kopia ax
	mov dx, ax
	
	; pierwszy bajt
	mov al, dh
	call fBajtNaHex
	
	mov byte ptr ds:[bx], ah
	inc bx
	mov byte ptr ds:[bx], al
	inc bx
	
	; drugi bajt
	mov al, dl
	call fBajtNaHex
	
	mov byte ptr ds:[bx], ah
	inc bx
	mov byte ptr ds:[bx], al
	inc bx
	
	; \r\n
	mov byte ptr ds:[bx], 0Dh
	inc bx
	mov byte ptr ds:[bx], 0Ah
	inc bx
	
	; int 21h, ah=40h - zapis do pliku
	mov ax, 4000h
	mov dx, bp	; dane do zapisu
	add dx, 8
	mov cx, bx	; ilo�� bajt�w do zapisu
	sub cx, dx
	mov bx, word ptr ds:[bp+2]	; uchwyt pliku
	int 21h
	jc fZapiszCRCBlad
	
	pop ax
	pop bx
	pop cx
	pop dx
	ret
	
	fZapiszCRCBlad:
		call fBajtNaHex
		mov bp, ax
		
		; int 21h ah=09h - wypisywanie stringa$
		mov ah, 09h
		mov dx, offset zBladZapiszPlik
		int 21h
		
		; wypisz uchwyt pliku
		mov ax, bx
		shr ax, 8
		call fBajtNaHex
		mov si, ax
		
		; pierwszy hex z file handle
		mov ax, si
		shr ax, 8
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h	
		
		; drugi hex z file handle
		mov ax, si
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		mov ax, bx
		call fBajtNaHex
		mov si, ax
		
		; trzeci hex z file handle
		mov ax, si
		shr ax, 8
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h	
		
		; czwarty hex z file handle
		mov ax, si
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, ':'
		int 21h	
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, ' '
		int 21h	
			
		; pierwszy hex z kodu b��du
		mov ax, bp
		shr ax, 8
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h	
		
		; drugi hex z kodu b��du
		mov ax, bp
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, al
		int 21h
		
		; int 21h ah=02h - wypisywanie bajtu
		mov ah, 02h
		mov dl, 'h'
		int 21h	
		
		mov ax, offset zBladPlik
		call fBlad
fZapiszCRC endp

fWczytajCRC proc	; out: CF - b��d?, ax - hash/kod b��du
	push cx
	push bx
	
	; sprawd� d�ugo�� linii (w bajtach)
	mov cx, word ptr ds:[bLiniaDlugosc]
	cmp cx, 4
		jne fWczytajCRCBladDlugosc
	
	; wczytaj hash
	mov bx, word ptr ds:[bLinia]
	; little endian
	
	xchg bh, bl
	
	call fHexNaLiczbe
		jc fWczytajCRCBladHex
	
	mov ah, al
	
	; wczytaj hash
	mov bx, word ptr ds:[bLinia+2]
	; little endian
	xchg bh, bl
	
	push ax
		
	call fHexNaLiczbe
		jc fWczytajCRCBladHex
	
	mov bl, al
	
	pop ax
	
	mov al, bl
	
	clc
	
	fWczytajCRCRet:
	pop bx
	pop cx
	ret
	
	fWczytajCRCBladHex:
		mov ax, offset zBladZnakHex
		stc
		jmp fWczytajCRCRet
	
	fWczytajCRCBladDlugosc:
		mov ax, offset zBladHashDlugosc
		stc
		jmp fWczytajCRCRet
fWczytajCRC endp

fWylicz proc
	pusha
	
	; pierwszy plik (dane)
	mov cx, word ptr ds:[zArgl]
	mov dx, offset zArgv
	mov bp, offset bBufor1
	call fOtworzPlikOdczyt
	
	; drugi plik (sumy kontrolne)
	add dx, cx	; dodaj d�ugo�� pierwszego parametru
	mov cx, word ptr ds:[zArgl + 2]
	mov bp, offset bBufor2
	call fOtworzPlikZapis
	
	; zapisz offset bufora 1
	mov bx, offset bBufor1
	
	; Licznik linii
	xor cx, cx
	
	fWyliczPetla:
		inc cx
		
		; Zamie� bufory i wczytaj lini�
		xchg bp, bx
		call fWczytajLinie
			jc fWyliczLiniaBlad
		
		; plik si� sko�czy�
		cmp ax, 1
			je fWyliczPetlaEnd
		
		; Wylicz CRC
		call fWyliczCRCLinia
		
		; Zamie� bufory i zapisz lini�
		xchg bp, bx
		call fZapiszCRC
		
		; Sprawd� czy licznik si� nie przekr�ci
		cmp cx, 65535
			jne fWyliczPetla
		
		; int 21h ah=09h - wypisywanie stringa$
		mov ax, 0900h
		mov dx, offset zBladIloscLinii
		int 21h
	fWyliczPetlaEnd:
	
	popa
	ret
	
	fWyliczLiniaBlad:
		; int 21h ah=09h - wypisywanie stringa$
		push ax
		
		mov ax, 0900h
		pop dx
		int 21h
		
		mov dx, offset zBladPlikWejsciowy
		int 21h
		
		mov ax, cx
		call fWypiszLiczbe
		
		mov ax, offset zBladPlik
		call fBlad
fWylicz endp

fWeryfikuj proc
	pusha
	
	; pierwszy plik (dane)
	mov cx, word ptr ds:[zArgl + 2]
	mov dx, offset zArgv + 2
	mov bp, offset bBufor1
	call fOtworzPlikOdczyt
	
	; drugi plik (sumy kontrolne)
	add dx, cx	; dodaj d�ugo�� pierwszego parametru
	mov cx, word ptr ds:[zArgl + 4]
	mov bp, offset bBufor2
	call fOtworzPlikOdczyt
	
	; zapisz offset bufora 1
	mov bx, offset bBufor1
	
	; licznik linii
	xor cx, cx
	
	fWeryfikujPetla:
		; Sprawd� czy licznik si� nie przekr�ci
		cmp cx, 65535
			je fWeryfikujIloscLiniiBlad
		
		inc cx
		
		; zamie� bufory i wczytaj lini� danych
		xchg bp, bx
		call fWczytajLinie
			jc fWeryfikujLiniaBlad
		
		; plik si� sko�czy�
		cmp ax, 1
			je fWeryfikujPetlaEnd
		
		; wylicz sum� kontroln�
		call fWyliczCRCLinia
			jc fWeryfikujLiniaBlad
		
		; zapisz wyliczony hash w dx
		mov dx, ax
		
		; zamie� bufory i wczytaj lini� z sum� kontroln�
		xchg bp, bx
		call fWczytajLinie
			jc fWeryfikujLiniaCRCBlad
		
		; plik si� sko�czy�, a nie powinien
		cmp ax, 1
			je fWeryfikujIloscLiniiCRCBlad
		
		; wczytaj CRC do ax
		call fWczytajCRC
			jc fWeryfikujLiniaCRCBlad
				
		cmp ax, dx
			je fWeryfikujPetla
		
		; hash si� nie zgadza
		mov ah, 09h
		mov dx, offset zBladHashNiezgodny
		int 21h
		
		mov ax, cx
		call fWypiszLiczbe
		
		mov ax, offset zBladPlik
		call fBlad
	fWeryfikujPetlaEnd:
	
	mov ax, 0900h
	mov dx, offset zOKSumy
	int 21h
	
	popa
	ret
	
	fWeryfikujIloscLiniiBlad:
		; int 21h ah=09h - wypisywanie stringa$
		mov ax, 0900h
		mov dx, offset zBladIloscLinii
		int 21h
		jmp fWeryfikujPetlaEnd
	
	fWeryfikujIloscLiniiCRCBlad:
		mov ax, offset zBladIloscLiniiCRC
	
	fWeryfikujLiniaCRCBlad:
		; int 21h ah=09h - wypisywanie stringa$
		push ax
		
		mov ax, 0900h
		pop dx
		int 21h
		
		mov dx, offset zBladPlikWejsciowyHash
		int 21h
		
		mov ax, cx
		call fWypiszLiczbe
		
		mov ax, offset zBladPlik
		call fBlad
	
	fWeryfikujLiniaBlad:
		; int 21h ah=09h - wypisywanie stringa$
		push ax
		
		mov ah, 09h
		pop dx
		int 21h
		
		mov dx, offset zBladPlikWejsciowy
		int 21h
		
		mov ax, cx
		call fWypiszLiczbe
		
		mov ax, offset zBladPlik
		call fBlad
fWeryfikuj endp

fBlad proc		; in: ax - offset ds:ax stringa$ z b��dem
	; int 21h ah=09h - wypisywanie stringa$
	push ax
	mov ah, 09h
	pop dx
	int 21h
	
	; int 21h ah=02h - wypisywanie bajtu (\r)
	mov ah, 02h
	mov dl, 0Dh		; \r
	int 21h
	
	; int 21h ah=02h - wypisywanie bajtu (\n)
	mov ah, 02h
	mov dl, 0Ah		; \n
	int 21h
	
	; int 21h ah=02h - wypisywanie bajtu (\r)
	mov ah, 02h
	mov dl, 0Dh		; \r
	int 21h
	
	; int 21h ah=02h - wypisywanie bajtu (\n)
	mov ah, 02h
	mov dl, 0Ah		; \n
	int 21h
	
	mov ah, 09h
	mov dx, offset zBladPomoc
	int 21h
	
	call fZamknijPliki
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4C01h
	int 21h
fBlad endp

code ends

data segment
	zArgv	db 256 dup(?)	; tablica sklejonych argument�w
	zArgc	dw 0		; ilo�� argument�w
	zArgl	dw 16 dup(?)	; tablica d�ugo�ci kolejnych argument�w
	
	zOtworzPlikNazwa	db 256 dup(?)
	zLiczbaWypisz		db '0', '0', '0', '0', '0', '$'
	
	zBladIloscArgumentow	db "B��d: program przyjmuje dwa lub trzy argumenty.$"
	zBlad3ArgArg1		db "B��d: przy wywo�aniu z trzema parametrami, pierwszy z nich musi by� flag� -v$"
	
	zBladZnakHex		db "B��d: znaleziono znak nieheksadecymalny w pliku $"
	zBladZnakCR				db "B��d: znaleziono \r bez \n$"
	zBladDlugoscLinii	db "B��d: przekroczono maksymaln� d�ugo�� linii (32kB) w pliku $"
	zBladDlugoscLiniiMod2	db "B��d: d�ugo�� linii nie jest parzysta w pliku $"
	zBladIloscLiniiCRC	db "B��d: brak wymaganej linii w pliku $"
	zBladHashDlugosc	db "B��d: z�a d�ugo�� linii (oczekiwano 4 bajt�w) w pliku $"
	
	zBladPlikWejsciowy	db "z danymi (<file>), linia: $"
	zBladPlikWejsciowyHash db "z sumami (<hash>), linia: $"
	
	zBladHashNiezgodny	db "B��d: Suma kontrolna niezgodna z danymi w linii $"
	zBladIloscLinii		db "Ostrze�enie: dzia�anie zako�czono na linii 65 535.",
							" Je�li pliki maj� wi�cej linii, to zosta�y one pomini�te." , 0Dh, 0Ah, 0Dh, 0Ah, '$'
	
	zBladOtworzPlik		db "B��d przy otwieraniu pliku $"
	zBladWczytajPlik	db "B��d przy odczytu z pliku FH=$"
	zBladZapiszPlik		db "B��d przy zapisie do pliku FH=$"
	zBladPlik			db "$"
	
	zOKSumy				db "Weryfikacja zako�czona pomy�lnie$"
	
	zBladPomoc	db "Spos�b u�ycia:", 0Dh, 0Ah,
					"   crc.exe <file> <hash>", 0Dh, 0Ah, "   crc.exe -v <file> <hash>", 0Dh, 0Ah, 0Dh, 0Ah,
					"gdzie <file> jest nazw� pliku, z kt�rego wyliczane b�d� sumy kontrolne,", 0Dh, 0Ah,
					"za� <hash> to plik, do kt�rego zapisane zostan� sumy CRC-16", 0Dh, 0Ah,
					"lub z kt�rym b�d� one por�wnywane (opcja -v)$"
	
	zOtwartyPlik	dw 0
	zOtwartePliki	dw 7 dup(?)
	
	; bufory do odczytu/zapisu pliku
	bBufor1			dw 4096
	bBufor1Uchwyt	dw ?
	bBufor1Czytaj	dw 0
	bBufor1Zapisz	dw 0
	bBufor1Dane		db 4096 dup(?)
	
	bBufor2			dw 4096
	bBufor2Uchwyt	dw ?
	bBufor2Czytaj	dw 0
	bBufor2Zapisz	dw 0
	bBufor2Dane		db 4096 dup(?)
	
	; bufor na wczytan� lini�
	bLinia			db 16384 dup(?)
	bLiniaDlugosc	dw ?
data ends


stack segment stack
		dw 1024 dup(?)
	czubek	db ?
stack ends

end program
