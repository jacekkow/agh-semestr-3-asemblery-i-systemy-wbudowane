.286

code segment

program:
	mov ax, stack	; inicjowanie stosu
	mov ss, ax
	mov sp, offset czubek
	
	mov ax, data	; inicjowanie danych
	mov ds, ax
	
	call fWczytajParametry
	call fSprawdzParametry
	call fPrzepiszHash
	call fHashNaBajty
	call fPrzetworzHash
	call fZamien
	call fWypisz
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4c00h
	int 21h
programEnd:

fBialyZnak proc	; in: al - znak, cl - ilo�� znak�w do przeczytania
		; out: ah - 0 (bia�y znak), 1 (nie-bia�y znak)
	cmp cl, 0
		je fBialyZnakTak
	cmp al, 20h	; SP
		je fBialyZnakTak
	cmp al, 00h	; \0
		je fBialyZnakTak
	cmp al, 09h	; \t
		je fBialyZnakTak
	cmp al, 0Ah	; \n
		je fBialyZnakTak
	cmp al, 0Dh	; \r
		je fBialyZnakTak
	
	fBialyZnakNie:
		mov ah, 1
		ret
	
	fBialyZnakTak:
		mov ah, 0
		ret
fBialyZnak endp

fNieBialyZnak proc
		; in: es:ax - adres w pami�ci, cl - maksymalna ilo�� bajt�w do przeczytania
		; out: es:ax - adres w pami�ci, cl - ilo�� bajt�w pozosta�a do przeczytania
	push si
	
	mov si, ax
	
	fNieBialyZnakPetla:
		mov al, byte ptr es:[si]
		
		call fBialyZnak
		cmp ah, 0
			je fNieBialyZnakBialy
		jmp fNieBialyZnakPetlaEnd
		
		fNieBialyZnakBialy:
			inc si
			loop fNieBialyZnakPetla
	fNieBialyZnakPetlaEnd:
	
	mov ax, si
	
	pop si
	ret	
fNieBialyZnak endp

fWczytajParametry proc
	pusha
	
	; ilo�� znak�w w parametrach linii komend
	xor cx, cx
	mov cl, byte ptr es:[80h]
	
	; pocz�tek parametr�w linii komend (es:81h)
	mov si, 81h
	
	; miejsce do zapisu parametr�w
	mov di, offset zArgv
	; miejsce do zapisu d�ugo�ci parametr�w
	mov bx, offset zArgl
	
	; czy mog� istnie� jakiekolwiek argumenty?
	cmp cl, 0
		je fWczytajParametryPetlaEnd
	
	; iteruj po kolejnych znakach linii komend
	fWczytajParametryPetla:
		; przejd� do pierwszego nie-bia�ego znaku
		mov ax, si
		call fNieBialyZnak
		mov si, ax
		
		; lArgl = 0 - ilo�� znak�w w aktualnym parametrze
		xor dx, dx
		
		; iteruj po kolejnych znakach parametru linii komend
		fWczytajParametryParametr:
			; al = aktualny znak parametru
			mov al, byte ptr es:[si]
			inc si
			
			; if whitespace(al): break;
			call fBialyZnak
			cmp ah, 0
				je fWczytajParametryPatametrEnd
			
			; zArgv[zArgc][di - zArgv[zArgc]] = al
			mov byte ptr ds:[di], al
			; di++
			inc di
			; lArgl++
			inc dx
			
			loop fWczytajParametryParametr
		fWczytajParametryPatametrEnd:
		
		; ostatnio przetwarzany parametr by� pusty - koniec
		cmp dx, 0
			je fWczytajParametryPetlaEnd
		
		; zArgl[bx] = lArgl
		mov word ptr ds:[bx], dx
		add bx, 2	; bx++
		
		; zArgc++
		mov ax, word ptr ds:[zArgc]
		inc ax
		mov word ptr ds:[zArgc], ax
		
		; osi�gni�to maksymaln� ilo�� parametr�w - koniec
		cmp ax, 16
			je fWczytajParametryPetlaEnd
		
		; wyczerpano ilo�� znak�w do przeczytania - koniec
		cmp cx, 0
			je fWczytajParametryPetlaEnd
		
		loop fWczytajParametryPetla
	fWczytajParametryPetlaEnd:
	
	popa
	ret
fWczytajParametry endp

fWypiszParametry proc
	pusha
	
	; wska�nik ma tablic� d�ugo�ci parametr�w (*zArgl)
	mov bp, offset zArgl
	
	; wska�nik na tablic� parametr�w (*zArgv)
	mov dx, offset zArgv
	
	; iteruj po kolejnych parametrach (zArgc)...
	mov cx, word ptr ds:[zArgc]
	; ...o ile w og�le istniej�
	cmp cx, 0
		je fWypiszParametryPetlaEnd
	
	fWypiszParametryPetla:
		push cx
		
		; int 21h ah=40h - wypisywanie danych
		mov ah, 40h
		mov bx, 0			; uchwyt pliku
		mov cx, word ptr ds:[zArgl+bp]	; liczba bajt�w do wypisania
		; mov dx, 			; dx - adres danych
		int 21h
		
		; we� d�ugo�� parametru
		mov cx, word ptr ds:[zArgl+bp]
		; przesu� "wska�nik" w dx (*zArgv) o d�ugo�� parametru
		add dx, cx
		
		call fWypiszCRLF
		
		; przesu� "wska�nik" w bp (*zArgl) o s�owo
		add bp, 2
		
		pop cx
		loop fWypiszParametryPetla
	fWypiszParametryPetlaEnd:
	
	popa
	ret
fWypiszParametry endp

fSprawdzParametr1 proc
	push ax
	
	; sprawd� d�ugo�� parametru
	mov ax, word ptr ds:[zArgl]
	cmp ax, 1
		jne fSprawdzParametr1Blad1
	
	; sprawd� czy parametr to 0 lub 1
	mov al, byte ptr ds:[zArgv]
	cmp al, '0'
		je fSprawdzParametr1OK
	cmp al, '1'
		je fSprawdzParametr1OK
	
	fSprawdzParametr1Blad1:
		mov ax, offset zBladArg1
		call fBlad
	
	fSprawdzParametr1OK:
		pop ax
		ret
fSprawdzParametr1 endp

fSprawdzParametr2Hex proc	; al - bajt do sprawdzenia
	; if al < '0': exception
	cmp al, '0'
		jl fSprawdzParametr2HexError1
	; if al <= '9': return
	cmp al, '9'
		jle fSprawdzParametr2OK
	; if al < 'A': exception
	cmp al, 'A'
		jl fSprawdzParametr2HexError1
	; if al <= 'F': return
	cmp al, 'F'
		jle fSprawdzParametr2OK
	; if al < 'a': exception
	cmp al, 'a'
		jl fSprawdzParametr2HexError1
	; if al <= 'f': return
	cmp al, 'f'
		jle fSprawdzParametr2OK
	
	fSprawdzParametr2OK:
		ret
	
	fSprawdzParametr2HexError1:
		mov ax, offset zBladArg2Hex
		call fBlad
fSprawdzParametr2Hex endp

fSprawdzParametr2 proc
	pusha
	
	; sprawd� d�ugo�� parametru
	mov ax, word ptr ds:[zArgl + 2]
	cmp ax, 47
		jne fSprawdzParametr2Error1
	
	; sprawd� bardziej znacz�ce p��bajty
	mov si, offset zArgv
	; pomi� pierwszy argument
	add si, 1
	
	; iteruj po p��bajtach
	mov cx, 16
	fSprawdzParametr2Petla1:
		mov al, byte ptr ds:[si]
		call fSprawdzParametr2Hex
		add si, 3 ; nast�pny p��bajt
		
		loop fSprawdzParametr2Petla1
	fSprawdzParametr2Petla1End:
	
	; sprawd� mniej znacz�ce p��bajty
	mov si, offset zArgv
	; pomi� pierwszy argument i bardziej znacz�cy p��bajt
	add si, 2
	
	; iteruj po p��bajtach
	mov cx, 16
	fSprawdzParametr2Petla2:
		mov al, byte ptr ds:[si]
		call fSprawdzParametr2Hex
		add si, 3 ; nast�pny p��bajt
		
		loop fSprawdzParametr2Petla2
	fSprawdzParametr2Petla2End:
	
	; sprawd� dwukropki
	mov si, offset zArgv
	mov ax, word ptr ds:[zArgl]
	add si, ax
	add si, 2
	
	mov cx, 15
	fSprawdzParametr2Petla3:
		mov al, byte ptr ds:[si]
		cmp al, ':'
			jne fSprawdzParametr2Error2
		add si, 3
		
		loop fSprawdzParametr2Petla3
	fSprawdzParametr2Petla3End:
	
	popa
	ret
	
	fSprawdzParametr2Error1:
		mov ax, offset zBladArg2Len
		call fBlad
	
	fSprawdzParametr2Error2:
		mov ax, offset zBladArg2Colon
		call fBlad
fSprawdzParametr2 endp

fSprawdzParametry proc
	push ax
	
	mov ax, word ptr ds:[zArgc]
	cmp ax, 2
		jne fSprawdzParametryBlad1
	
	call fSprawdzParametr1
	call fSprawdzParametr2
	
	pop ax
	ret
	
	fSprawdzParametryBlad1:
		mov ax, offset zBladArgNum
		call fBlad
fSprawdzParametry endp

fPrzepiszHash proc
	pusha
	
	mov si, offset zArgv
	add si, 1	; pomi� pierwszy argument
	
	mov di, offset zHashT
	
	mov cx, 16
	fPrzepiszHashPetla:
		mov al, byte ptr ds:[si]
		
		cmp al, '9'
			jle fPrzepiszHashPetlaOK1
		cmp al, 'a'
			jge fPrzepiszHashPetlaOK1
		add al, 20h	; A -> a, B -> b, ...
		fPrzepiszHashPetlaOK1:
		
		mov byte ptr ds:[di], al
		inc si
		inc di
		
		mov al, byte ptr ds:[si]
		
		cmp al, '9'
			jle fPrzepiszHashPetlaOK2
		cmp al, 'a'
			jge fPrzepiszHashPetlaOK2
		add al, 20h	; A -> a, B -> b, ...
		fPrzepiszHashPetlaOK2:
		
		mov byte ptr ds:[di], al
		add si, 2	; pomi� :
		inc di
		
		loop fPrzepiszHashPetla
	fPrzepiszHashPetlaEnd:
	
	popa
	ret
fPrzepiszHash endp

fHashNaBajty proc	; zamienia hash zHashT na bajty w zHash
	pusha
	
	mov si, offset zHashT
	mov di, offset zHash
	mov cx, 16 ; iteracja po bajtach hasha tekstowego
	
	fHashNaBajtyPetla:
		; bardziej znacz�cy p��bajt
		mov ah, byte ptr ds:[si]
		; mniej znacz�cy p��bajt
		mov al, byte ptr ds:[si+1]
		add si, 2
		
		sub ah, '0'
		cmp ah, 10
			jl fHashNaBajtyAHOK
		
		sub ah, 27h
		
		fHashNaBajtyAHOK:
		
		sub al, '0'
		cmp al, 10
			jl fHashNaBajtyALOK
		
		sub al, 27h
		
		fHashNaBajtyALOK:
		
		; ah << 4
		shl ah, 4
		; al = al | ah
		or al, ah
		
		; zapisz bajt w zHash
		mov byte ptr ds:[di], al
		inc di
		
		loop fHashNaBajtyPetla
	fHashNaBajtyPetlaEnd:
	
	popa
	ret
fHashNaBajty endp

fPrzeliczWspolrzedne proc
			; in: ah - x, al - y
			; out: ax - przesuni�cie w zDane
	push bx
	push cx
	
	mov bx, ax
	
	; ax = al * 17 (bajt*bajt)
	xor ah, ah
	mov cl, 17
	mul cl
	
	; ax = ax + bh
	xor cx, cx
	mov cl, bh
	add ax, cx
	
	pop cx
	pop bx
	ret
fPrzeliczWspolrzedne endp

fWykonajRuch proc	; in: ax mod 4 = 0,1,2,3 - ruch
	pusha
	
	; wczytaj obecne wsp��rz�dne
	mov bx, word ptr ds:[zPozycja]
	
	mov cx, ax	; skopiuj ruch
	
	; if (bit(cx, 15) = 0): gora()
	;	else: dol()
	and cx, 2
		jz fWykonajRuchJezeli1Gora
	fWykonajRuchJezeli1Dol:
		; if bl = 8: break;
		;	else: bl++
		cmp bl, 8
			je fWykonajRuchJezeli1End
		inc bl
		
		jmp fWykonajRuchJezeli1End
	fWykonajRuchJezeli1Gora:
		; if bl = 0: break;
		;	else: bl--
		cmp bl, 0
			je fWykonajRuchJezeli1End
		dec bl
	fWykonajRuchJezeli1End:
	
	mov cx, ax	; skopiuj ruch
	
	; if (bit(cx, 16) = 0): lewo()
	;	else: prawo()
	and cx, 1
		jz fWykonajRuchJezeli2Lewo
	fWykonajRuchJezeli2Prawo:
		; if bh = 16: break;
		;	else: bh++
		cmp bh, 16
			je fWykonajRuchJezeli2End
		inc bh
		
		jmp fWykonajRuchJezeli2End
	fWykonajRuchJezeli2Lewo:
		; if bh = 0; break;
		;	else bh--
		cmp bh, 0
			je fWykonajRuchJezeli2End
		dec bh
	fWykonajRuchJezeli2End:
	
	; zapisz nowe wsp��rz�dne
	mov word ptr ds:[zPozycja], bx
	
	; przelicz wsp��rz�dne
	mov ax, bx
	call fPrzeliczWspolrzedne
	mov bx, ax
	
	; zwi�ksz licznik odwiedzin
	mov al, byte ptr ds:[zDane+bx]
	inc al
	mov byte ptr ds:[zDane+bx], al
	
	popa
	ret
fWykonajRuch endp

fPrzetworzBajt proc	; in: al - bajt do przetworzenia
	push ax
	push cx
	
	mov cx, 4	; iteracja po parach bit�w
	fPrzetworzBajtPetla:
		call fWykonajRuch
		shr al, 2	; al >> 2
		
		loop fPrzetworzBajtPetla
	fPrzetworzBajtPetlaEnd:
	
	pop cx
	pop ax
	ret
fPrzetworzBajt endp

fPrzetworzBajtMod proc	; in: al - bajt do przetworzenia
	push ax
	push cx
	
	mov cx, 4	; iteracja po parach bit�w
	fPrzetworzBajtModPetla:
		rol al, 2	; al << 2 << al
		call fWykonajRuch
		
		loop fPrzetworzBajtModPetla
	fPrzetworzBajtModPetlaEnd:
	
	pop cx
	pop ax
	ret
fPrzetworzBajtMod endp

fPrzetworzHash proc
	pusha
	
	mov bl, byte ptr ds:[zArgv]
	mov cx, 16		; iteracja po bajtach z Hash
	mov si, offset zHash	; adres bajtu
	
	fPrzetworzHashPetla:
		mov al, byte ptr ds:[si]
		
		; modyfikacja?
		cmp bl, '1'
			je fPrzetworzHashMod
		
		fPrzetworzHashNormal:
			call fPrzetworzBajt
			jmp fPrzetworzHashModEnd
		fPrzetworzHashMod:
			call fPrzetworzBajtMod
		fPrzetworzHashModEnd:
		
		inc si
		loop fPrzetworzHashPetla
	fPrzetworzHashPetlaEnd:
	
	popa
	ret
fPrzetworzHash endp

fZamien proc	; zamienia ilo�ci wyst�pie� w zDane na znaki z zZnaki
	pusha
	
	xor ax, ax		; zeruj AX
	mov cx, 153		; ilo�� obrot�w p�tli
	mov si, offset zZnaki	; adres znak�w
	mov di, offset zDane	; adres danych
	
	fZamienPetla:
		; wczytaj licznik odwiedzin
		mov al, byte ptr ds:[di]
		
		; if al > 14; al = 14
		cmp al, 14
			jle fZamienPorownane
			mov al, 14
		fZamienPorownane:
		
		; przesuni�cie w tablicy znak�w
		mov bx, ax
		; zamie� liczb� na znak
		mov al, byte ptr ds:[zZnaki + bx]
		mov byte ptr ds:[di], al
		
		; przesu� si� w tablicy danych
		inc di
		
		; sprawd� warunek p�tli
		loop fZamienPetla
	fZamienPetlaEnd:
	
	; ustawienie znacznika pocz�tku
	mov byte ptr ds:[zDane + 76], 'S'
	
	; ustawienie znacznika ko�ca
	mov ax, word ptr ds:[zPozycja]
	call fPrzeliczWspolrzedne
	mov bx, ax
	mov byte ptr ds:[zDane + bx], 'E'
	
	popa
	ret
fZamien endp

fWypiszCRLF proc	; wypisuje \r\n
	push ax
	push dx
	
	; int 21h ah=02h - wypisywanie bajtu (\r)
	mov ah, 02h
	mov dl, 0Dh		; \r
	int 21h
	
	; int 21h ah=02h - wypisywanie bajtu (\n)
	mov ah, 02h
	mov dl, 0Ah		; \n
	int 21h
	
	pop dx
	pop ax
	ret
fWypiszCRLF endp

fWypiszAL proc	; wypisuje znak z AL
	push dx
	
	mov ah, 02h
	mov dl, al
	int 21h			; int 21h ah=02h - wypisywanie bajtu
	
	pop dx
	ret
fWypiszAL endp

fWypisz proc	; wypisuje tablic� zDane
	pusha
	
	; wypisz pocz�tek ramki
	mov ah, 09h
	mov dx, offset zRamka
	int 21h			; int 21h ah=09h - wypisywanie stringa$
	
	; wpisz \r\n
	call fWypiszCRLF
	
	; inicjowanie p�tli
	mov cx, 9
	mov dx, offset zDane

	; wypisywanie jednej linii symboli
	fWypiszPetla:
		mov al, '|'
		call fWypiszAL
		
		push cx
		
		; int 21h ah=40h - wypisywanie danych
		mov ah, 40h
		mov bx, 0		; uchwyt pliku
		mov cx, 17		; liczba bajt�w do wypisania
		; mov dx, 		; dx - adres danych
		int 21h
		
		add dx, 17
		pop cx
		
		mov al, '|'
		call fWypiszAL
		
		call fWypiszCRLF
		
		; if cx > 0: continue
		loop fWypiszPetla
	fWypiszPetlaEnd:
	
	; int 21h ah=09h - wypisywanie stringa$
	mov ah, 09h
	mov dx, offset zRamka
	int 21h
	
	popa
	ret
fWypisz endp

fBlad proc		; in: ax - offset ds:ax stringa$ z b��dem
	; int 21h ah=09h - wypisywanie stringa$
	push ax
	mov ah, 09h
	pop dx
	int 21h
	
	call fWypiszCRLF
	call fWypiszCRLF
	
	mov ah, 09h
	mov dx, offset zBladPomoc
	int 21h
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4c01h
	int 21h
fBlad endp

code ends

data segment
	zArgv	db 256 dup(?)	; tablica sklejonych argument�w
	zArgc	dw 0		; ilo�� argument�w
	zArgl	dw 16 dup(?)	; tablica d�ugo�ci kolejnych argument�w
	
	zHashT	db 32 dup(?)	; hash jako lowercasemd5
	zHash	db 16 dup(?)	; hash jako bajty
	
	zZnaki	db " .o+=*BOX@%&#/^"
	zRamka	db "+-----------------+$"
	
	zPozycja dw 0804h	; high - x, low - y
	zDane	db 153 dup(0)	; ilo�ci wyst�pie� go�ca
	
	zBlad		db "B��d: nieznany problem$"
	zBladArgNum	db "B��d: z�a liczba argument�w (oczekiwano dw�ch)$"
	zBladArg1	db "B��d: pierwszy parametr winien by� r�wny 0 lub 1$"
	zBladArg2Len	db "B��d: drugi parametr winien by� hashem (ma z�� d�ugo��)$"
	zBladArg2Hex	db "B��d: drugi parametr winien by� hashem (znaleziono znak nie heksadecymalny)$"
	zBladArg2Colon	db "B��d: drugi parametr winien by� hashem (nie znaleziono oczekiwanego dwukropka)$"
	zBladPomoc	db "Spos�b u�ycia:", 0Ah, 0Dh,
			   "   prog.exe <flaga> <hash>", 0Ah, 0Dh, 0Ah, 0Dh,
			   "gdzie <flaga> jest znakiem 0 lub 1 i oznacza:", 0Ah, 0Dh,
			   "   0 - program dzia�a normalnie,", 0Ah, 0Dh,
			   "   1 - program dzia�a anormalnie,", 0Ah, 0Dh,
			   "za� <hash> to skr�t MD5, w kt�rym poszczeg�lne bajty",
			   " s� rozdzielone dwukropkami.$"
data ends

stack segment stack
		dw 1024 dup(?)
	czubek	db ?
stack ends

end program
