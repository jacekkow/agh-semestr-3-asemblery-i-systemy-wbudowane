.286
.8087

code segment

program:
	mov ax, stack	; inicjowanie stosu
	mov ss, ax
	mov sp, offset czubek
	
	mov ax, data	; inicjowanie danych
	mov ds, ax
	
	call fWczytajParametry
	
	finit		; inicjowanie koprocesora
	
	call fSprawdzParametry
	
	
	call trybGraficzny
	call rysuj
	call czekajNaZnak
	
	call trybTekstowy
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4c00h
	int 21h
programEnd:


fBialyZnak proc	; in: al - znak, cx - ilo�� znak�w do przeczytania
		; out: ah - 0 (bia�y znak), 1 (nie-bia�y znak)
	cmp cx, 0
		je fBialyZnakTak
	cmp al, 20h	; SP
		je fBialyZnakTak
	cmp al, 00h	; \0
		je fBialyZnakTak
	cmp al, 09h	; \t
		je fBialyZnakTak
	cmp al, 0Ah	; \n
		je fBialyZnakTak
	cmp al, 0Dh	; \r
		je fBialyZnakTak
	
	fBialyZnakNie:
		mov ah, 1
		ret
	
	fBialyZnakTak:
		mov ah, 0
		ret
fBialyZnak endp

fNieBialyZnak proc
		; in: es:ax - adres w pami�ci, cx - maksymalna ilo�� bajt�w do przeczytania
		; out: es:ax - adres w pami�ci, cx - ilo�� bajt�w pozosta�a do przeczytania
	push si
	
	mov si, ax
	
	fNieBialyZnakPetla:
		mov al, byte ptr es:[si]
		
		call fBialyZnak
		cmp ah, 0
			je fNieBialyZnakBialy
		jmp fNieBialyZnakPetlaEnd
		
		fNieBialyZnakBialy:
			inc si
			loop fNieBialyZnakPetla
	fNieBialyZnakPetlaEnd:
	
	mov ax, si
	
	pop si
	ret	
fNieBialyZnak endp

fWczytajParametry proc
	pusha
	
	; ilo�� znak�w w parametrach linii komend
	xor cx, cx
	mov cl, byte ptr es:[80h]
	
	; pocz�tek parametr�w linii komend (es:81h)
	mov si, 81h
	
	; miejsce do zapisu parametr�w
	mov di, offset zArgv
	; miejsce do zapisu d�ugo�ci parametr�w
	mov bx, offset zArgl
	
	; czy mog� istnie� jakiekolwiek argumenty?
	cmp cl, 0
		je fWczytajParametryPetlaEnd
	
	; iteruj po kolejnych znakach linii komend
	fWczytajParametryPetla:
		; przejd� do pierwszego nie-bia�ego znaku
		mov ax, si
		call fNieBialyZnak
		mov si, ax
		
		; lArgl = 0 - ilo�� znak�w w aktualnym parametrze
		xor dx, dx
		
		; iteruj po kolejnych znakach parametru linii komend
		fWczytajParametryParametr:
			; al = aktualny znak parametru
			mov al, byte ptr es:[si]
			inc si
			
			; if whitespace(al): break;
			call fBialyZnak
			cmp ah, 0
				je fWczytajParametryPatametrEnd
			
			; zArgv[zArgc][di - zArgv[zArgc]] = al
			mov byte ptr ds:[di], al
			; di++
			inc di
			; lArgl++
			inc dx
			
			loop fWczytajParametryParametr
		fWczytajParametryPatametrEnd:
		
		; ostatnio przetwarzany parametr by� pusty - koniec
		cmp dx, 0
			je fWczytajParametryPetlaEnd
		
		; zArgl[bx] = lArgl
		mov word ptr ds:[bx], dx
		add bx, 2	; bx++
		
		; zArgc++
		mov ax, word ptr ds:[zArgc]
		inc ax
		mov word ptr ds:[zArgc], ax
		
		; osi�gni�to maksymaln� ilo�� parametr�w - koniec
		cmp ax, 16
			je fWczytajParametryPetlaEnd
		
		; wyczerpano ilo�� znak�w do przeczytania - koniec
		cmp cx, 0
			je fWczytajParametryPetlaEnd
		
		loop fWczytajParametryPetla
	fWczytajParametryPetlaEnd:
	
	popa
	ret
fWczytajParametry endp

fSprawdzParametry proc
	pusha
	
	mov ax, word ptr ds:[zArgc]
	mov si, offset zArgv
	mov di, offset zArgl
	
	cmp ax, 4
		jg fSprawdzIloscParametrow
	
	cmp ax, 1
		jl fSprawdzParametrySprawdzone
	
	; pierwszy parametr
	
	; tekst ewentualnego b��du
	mov word ptr ds:[eBlad], offset eParam1
	
	; offset do zapisu
	mov bx, offset lXmin
	; d�ugo�� argumentu
	mov cx, word ptr ds:[di]
	call wczytaj
	
	; aktualny parametr = poprzedni parametr + d�ugo��
	add si, cx
	; argl++
	add di, 2
	
	cmp ax, 2
		jl fSprawdzParametrySprawdzone
	
	; drugi parametr
	
	; tekst ewentualnego b��du
	; d�ugo�� argumentu
	mov word ptr ds:[eBlad], offset eParam2
	
	; offset do zapisu
	mov bx, offset lXmax
	mov cx, word ptr ds:[di]
	call wczytaj
	
	; aktualny parametr = poprzedni parametr + d�ugo��
	add si, cx
	; argl++
	add di, 2
	
	cmp ax, 3
		jl fSprawdzParametrySprawdzone
	
	; trzeci parametr
	
	; tekst ewentualnego b��du
	mov word ptr ds:[eBlad], offset eParam3
	
	; offset do zapisu
	mov bx, offset lYmin
	; d�ugo�� argumentu
	mov cx, word ptr ds:[di]
	call wczytaj
	
	; aktualny parametr = poprzedni parametr + d�ugo��
	add si, cx
	; argl++
	add di, 2
	
	cmp ax, 4
		jl fSprawdzParametrySprawdzone
	
	; czwarty parametr
	
	; tekst ewentualnego b��du
	mov word ptr ds:[eBlad], offset eParam4
	
	; offset do zapisu
	mov bx, offset lYmax
	; d�ugo�� argumentu
	mov cx, word ptr ds:[di]
	call wczytaj
	
	; aktualny parametr = poprzedni parametr + d�ugo��
	add si, cx
	; argl++
	add di, 2
	
	fSprawdzParametrySprawdzone:
	
	popa
	ret
	
	fSprawdzIloscParametrow:
		mov dx, offset eBladIP
		call fBlad
fSprawdzParametry endp

wczytaj proc ; in: ds:bx - offset do zapisu liczby zmiennopozycyjnej (10B)
			; si - pocz�tek stringa, cx - ilo�� bajt�w
	pusha
	
	; di - offset za ostatni znak
	mov di, si
	add di, cx
	
	; czy pusta warto��?
	cmp cx, 0
		je wczytajRet
	
	mov word ptr ds:[zBuforS], si
	mov word ptr ds:[zBuforF], di
	
	call sprawdzLiczbe
	call zapiszLiczbeBCD
	
	fstp tbyte ptr ds:[bx]
	
	wczytajRet:
	popa
	ret
wczytaj endp

fBlad proc ; in: dx - offset do b��du$
	call wypisz
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4c01h
	int 21h
fBlad endp

wypisz proc ; in: ds:dx - offset do stringa$
	; int 21h ah=09 - wypisywanie stringa$
	mov ax, 0900h
	int 21h
	
	ret
wypisz endp

blad proc ; in: ax - offset do b��du$
	push ax
	
	mov dx, offset zInfo
	call wypisz
	
	mov dx, word ptr ds:[eBlad]
	call wypisz
	
	pop dx
	call wypisz
	
	; int 21h ah=4c - powr�t do systemu operacyjnego
	mov ax, 4c01h
	int 21h
blad endp

sprawdzLiczbe proc
	pusha
	
	; offset do bufora
	mov si, word ptr ds:[zBuforS]
	; offset do pocz�tku cyfr
	mov bx, si
	
	; offset do ko�ca bufora
	mov di, word ptr ds:[zBuforF]
	
	; offset do kropki dziesi�tnej
	xor dx, dx
	
	; pierwszy bajt mo�e by� minusem
	mov al, byte ptr ds:[si]
	cmp al, '-'
		jne sprawdzLiczbePetla1
	
	inc bx
	inc si
	
	; sprawd� cz�� ca�kowit�
	sprawdzLiczbePetla1:
		mov al, byte ptr ds:[si]
		
		cmp al, '.'
			je sprawdzLiczbePetla1End
		cmp al, '0'
			jl sprawdzBladKropka
		cmp al, '9'
			jg sprawdzBladKropka
		
		inc si
		cmp si, di
			je sprawdzPetla1EOL
		jmp sprawdzLiczbePetla1
		
		sprawdzPetla1EOL:
			mov dx, si
			jmp sprawdzZnakiSprawdzone
	sprawdzLiczbePetla1End:
	
	; mamy kropk�
	mov dx, si
	inc si
	
	; doszli�my do ko�ca
	cmp si, di
		je sprawdzZnakiSprawdzone
	
	; sprawd� cz�� u�amkow�
	sprawdzLiczbePetla2:
		mov al, byte ptr ds:[si]
		cmp al, '0'
			jl sprawdzBladZnak
		cmp al, '9'
			jg sprawdzBladZnak
		
		inc si
		cmp si, di
			jne sprawdzLiczbePetla2
	sprawdzLiczbePetla2End:
	
	sprawdzZnakiSprawdzone:
	
	; ilo�� cyfr w cz�sci ca�kowitej
	cmp bx, dx
		je sprawdzBladCalkowita
	
	; ilo�� cyfr w cz�ci u�amkowej (-1, bo '.')
	mov ax, di
	sub ax, dx
	dec ax
		jz sprawdzBladUlamkowa
	
	; sprawdz ilosc cyfr
	cmp di, dx
		jne sprawdzIlosc
	
	inc ax
	
	sprawdzIlosc:
	add ax, dx
	sub ax, bx
	
	; sprawd� dok�adno��
	cmp ax, 18
		jg sprawdzBladIlosc
	
	; zapisz dane
	mov word ptr ds:[zBuforC], bx
	mov word ptr ds:[zBuforK], dx
	
	popa
	
	ret
	
	sprawdzBladZnak:
		mov ax, offset eBlad1
		call blad
	
	sprawdzBladKropka:
		mov dl, 13
		mov ah, 02h
		int 21h
		
		mov dl, 10
		mov ah, 02h
		int 21h
		
		mov ax, offset eBlad2
		call blad
	
	sprawdzBladCalkowita:
		mov ax, offset eBlad3
		call blad
	
	sprawdzBladUlamkowa:
		mov ax, offset eBlad4
		call blad
		
	sprawdzBladIlosc:
		mov ax, offset eBlad5
		call blad
sprawdzLiczbe endp

zapiszBCD proc ; in: ax - dwa bajty cyfr, bp - adres do zapisu
	push ax
	
	sub ah, '0'
	shl ah, 4
	
	sub al, '0'
	
	or al, ah
	
	mov byte ptr ds:[bp], al
	inc bp
	
	pop ax
	ret
zapiszBCD endp

zapiszLiczbeBCD proc ; out: st(0) - liczba
	pusha
	push es
	
	; zeruj miejsce na BCD
	mov ax, ds
	mov es, ax
	; miejsce do zapisu BCD (10B)
	mov di, offset zBCD
	
	mov cx, 5
	xor ax, ax
	rep stosw
	
	; offset bufora
	mov si, word ptr ds:[zBuforS]
	; wska�nik na pierwsz� cyfr�
	mov bx, word ptr ds:[zBuforC]
	; wska�nik na kropk� dziesi�tn�
	mov dx, word ptr ds:[zBuforK]
	; wska�nik za ostatni� cyfr�
	mov di, word ptr ds:[zBuforF]
	
	; miejsce do zapisu BCD (10B)
	mov bp, offset zBCD
	
	; czy jest znak (-)?
	cmp si, bx
		je zapiszDodatnia
	
	; zapisz znak
	mov byte ptr ds:[bp+9], 80h
	
	zapiszDodatnia:
	
	; dane do zapisu
	xor ax, ax
	
	; zaczynamy od ko�ca
	mov si, di
	
	; czy s� jakie� znaki w cz�ci u�amkowej?
	cmp di, dx
		je zapiszLiczbePetla2
	
	; tak, s�, p�tla po cyfrach cz�ci u�amkowej
	zapiszLiczbePetla1:
		; przesu� wska�nik i sprawd�, czy nie kropka
		dec si
		cmp si, dx
			je zapiszLiczbePetla1End
		
		; ah przenie� do al, wczytaj bajt do al
		xchg ah, al
		mov ah, byte ptr ds:[si]
		
		; wczytali�my dwa bajty?
		cmp al, 0
			je zapiszLiczbePetla1
		
		; tak, zapiszmy je w kodzie BCD
		call zapiszBCD
		inc bp
		xor ax, ax
		
		jmp zapiszLiczbePetla1
	zapiszLiczbePetla1End:
	
	; p�tla po cyfrach cz�ci ca�kowitej
	zapiszLiczbePetla2:
		; przesu� wska�nik i sprawd�, czy nie ostatnia cyfra
		dec si
		cmp si, bx
			jl zapiszLiczbePetla2End
		
		; ah przenie� do al, wczytaj bajt do al
		xchg ah, al
		mov ah, byte ptr ds:[si]
		
		; wczytali�my dwa bajty?
		cmp al, 0
			je zapiszLiczbePetla2
		
		; tak, zapiszmy je w kodzie BCD
		call zapiszBCD
		xor ax, ax
		
		jmp zapiszLiczbePetla2
	zapiszLiczbePetla2End:
	
	; zosta� jaki� bajt do zapisu?
	cmp ax, 0
		je zapiszZapisana
	
	; tak, zapiszmy go
	xchg ah, al
	mov ah, '0'
	call zapiszBCD
	xor ax, ax
	
	zapiszZapisana:
	
	; ile razy mamy dzieli� przez 10?
	mov cx, di
	sub cx, dx
		jz zapiszKonwersja
	
	; kropka nie zwi�ksza ilo�ci dziele�
	dec cx
	
	zapiszKonwersja:
	;
	fild word ptr ds:[lDziesi]
	; 10
	fbld tbyte ptr ds:[zBCD]
	; t, 10
	
	zapiszKonwersjaDziel:
		cmp cx, 0
			je zapiszKonwersjaDzielEnd
		
		; t, 10
		fdiv st(0), st(1)
		; t/10, 10
		
		dec cx
		jmp zapiszKonwersjaDziel
	zapiszKonwersjaDzielEnd:
	
	; t/10, t
	ffree st(1)
	; t/10
	
	pop es
	popa
	ret
zapiszLiczbeBCD endp

czekajNaZnak proc
	mov ax, 0800h
	int 21h
	
	ret
czekajNaZnak endp

trybGraficzny proc
	; inicjowanie trybu graficznego
	mov ax, 0013h
	int 10h
	
	ret
trybGraficzny endp

trybTekstowy proc
	; inicjowanie trybu tekstowego
	mov ax, 0003h
	int 10h
	
	ret
trybTekstowy endp

rysuj proc
	pusha
	
	; adresy w pami�ci
	mov ax, 0a000h
	mov es, ax
	mov di, 0000h
	
	; bx - wsp�rz�dna x
	; dx - wsp�rz�dna y
	
	xor dx, dx ; y = 0
	rysujYPetla:
		xor bx, bx ; x = 0
		
		; wylicz q
		mov word ptr ds:[lWspol], dx
		;
		fld tbyte ptr ds:[lYmin]
		; ymin
		fld tbyte ptr ds:[lYmax]
		; ymax, ymin
		fsub st(0), st(1)
		; ymax-ymin, ymin
		fimul word ptr ds:[lWspol]
		; M*(ymax-ymin), ymin
		fidiv word ptr ds:[lRozdY]
		; M*(ymax-ymin)/rozdY, ymin
		faddp st(1), st(0)
		; ymin + M*(ymax-ymin)/rozdY
		
		rysujXPetla:
			mov cx, 1001 ; i = 1001
			
			; wylicz p
			mov word ptr ds:[lWspol], bx
			; q
			fld tbyte ptr ds:[lXmin]
			; xmin, q
			fld tbyte ptr ds:[lXmax]
			; xmax, xmin, q
			fsub st(0), st(1)
			; xmax-xmin, xmin, q
			fimul word ptr ds:[lWspol]
			; N*(xmax-xmin), xmin, q
			fidiv word ptr ds:[lRozdX]
			; N*(xmax-xmin)/rozdX, xmin, q
			faddp st(1), st(0)
			; xmin + N*(xmax-xmin)/rozdX, q
			; p, q
			
			fldz ; wart y
			fldz ; wart x
			; x, y, p, q
			
			rysujIPetla:
				; x, y, p, q
				fld st(1)
				; y, x, y, p, q
				fmul st(0), st(0)
				; y*y, x, y, p, q
				fld st(1)
				; x, y*y, x, y, p, q
				fmul st(0), st(0)
				; x*x, y*y, x, y, p, q
				faddp st(1), st(0)
				; x*x+y*y, x, y, p, q
				ficomp word ptr ds:[lCztery]
				; x, y, p, q
				
				; rejestr flag FPU -> AX (AH)
				fstsw ax
				; czekaj na zako�czenie
				fwait
				
				; AH -> rejestr flag CPU
				sahf
					ja rysujIPetlaEnd
				
				dec cx
					jz rysujIPetlaEnd
				
				; x, y, p, q
				fld st(0)
				; x, x, y, p, q
				fmul st(0), st(0)
				; x*x, x, y, p, q
				fxch st(2)
				; y, x, x*x, p, q
				fld st(0)
				; y, y, x, x*x, p, q
				fld1
				; 1, y, y, x, x*x, p, q
				fadd st(0), st(0)
				; 2, y, y, x, x*x, p, q
				fmulp st(1), st(0)
				; 2*y, y, x, x*x, p, q
				fmulp st(2), st(0)
				; y, 2*x*y, x*x, p, q
				fmul st(0), st(0)
				; y*y, 2*x*y, x*x, p, q
				fsubp st(2), st(0)
				; 2*x*y, x*x-y*y, p, q
				fsub st(0), st(3)
				; 2*x*y+q, x*x-y*y, p, q
				fxch st(1)
				; x*x-y*y, 2*x*y+q, p, q
				fadd st(0), st(2)
				; x*x-y*y+p, 2*x*y+q, p, q
				
				jmp rysujIPetla
			rysujIPetlaEnd:
			
			; usu� x, y, p
			
			; x*x-y*y+p, 2*x*y+q, p, q
			fxch st(3)
			; q, 2*x*y+q, p, x*x-y*y+p
			ffree st(3)
			; q, 2*x*y+q, p
			ffree st(2)
			; q, 2*x*y+q
			ffree st(1)
			; q
			
			cmp cx, 0
				je rysujXPetlaBialy
			
			; cx != 0 (czarny - indeks 0)
			xor cx, cx
			jmp rysujXPetlaVGA
			
			rysujXPetlaBialy:
			; cx = 0 (bia�y - indeks 15)
			mov cx, 15
			
			rysujXPetlaVGA:
			mov byte ptr es:[di], cl
			inc di
			
			inc bx
			cmp bx, 320
				jne rysujXPetla
		rysujXPetlaEnd:
		
		; usu� q
		ffree st(0)
		
		inc dx
		cmp dx, 200
			jne rysujYPetla
	rysujYPetlaEnd:
	
	popa
	ret
rysuj endp

code ends

data segment
	zArgv	db 256 dup(?)	; tablica sklejonych argument�w
	zArgc	dw 0		; ilo�� argument�w
	zArgl	dw 16 dup(?)	; tablica d�ugo�ci kolejnych argument�w
	
	zBuforS dw ?		; pozycja pocz�tku bufora
	zBuforC	dw ?		; pozycja pierwszej cyfry
	zBuforK	dw ?		; pozycja kropki w buforze
	zBuforF	dw ?		; pozycja za ostatni� cyfr�
	
	zBCD	dt 10 dup(?)
	
	eParam1	db "Parametr 1 - b��dny format: $"
	eParam2	db "Parametr 2 - b��dny format: $"
	eParam3	db "Parametr 3 - b��dny format: $"
	eParam4	db "Parametr 4 - b��dny format: $"
	
	eBlad	dw ?
	
	eBlad1	db "oczekiwano liczby$"
	eBlad2	db "oczekiwano liczby lub znaku '.'$"
	eBlad3	db "cz�� ca�kowita nie mo�e by� pusta$"
	eBlad4	db "cz�� u�amkowa nie mo�e by� pusta$"
	eBlad5	db "mo�e mie� najwy�ej 18 miejsc znacz�cych$"
	eBlad6	db "oczekiwano maksymalnie 20 znak�w$"
	eBladIP	db "Oczekiwano maksymalnie 4 parametr�w$"
	
	zInfo	db "mand.exe [xmin [xmax [ymin [ymax]]]]", 13, 10,
				"   rysuje fraktal Mandelbrota o zadanych wsp�rz�dnych", 13, 10,
				13, 10,
				"Domy�lnie: xmin=-2, xmax=1, ymin=-1, ymax=1", 13, 10,
				13, 10, '$'
	
	lWspol	dw ?
	lCztery	dw 4
	lDziesi	dw 10
	lRozdX	dw 320
	lRozdY	dw 200
	
	lXmin	dt -2.0
	lXmax	dt 1.0
	lYmin	dt -1.0
	lYmax	dt 1.0
	
data ends

stack segment stack
		dw 1024 dup(?)
	czubek	db ?
stack ends

end program
